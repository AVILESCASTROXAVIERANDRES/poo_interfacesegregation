﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programa_Con_IS
{
    class Celular_1:IOperaciones
    {
        // se utilizan los metodos que solo necesita que en este caso son todos 
        public void Presentacion()
        {
            Console.WriteLine("El celular Samsung: ");
        }
        public void Division()
        {
            Console.WriteLine("Puede realizar una división");
        }

        public void Resta()
        {
            Console.WriteLine("Puede realizar una resta");
        }

        public void Suma()
        {
            Console.WriteLine("Puede realizar una suma");
        }
    }
}
