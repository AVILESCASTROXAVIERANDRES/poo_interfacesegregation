﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programa_Con_IS
{
    class Celular_2:ICelular_2 //se implementa la interfaz que se necesita que es la del celular 2
    {
        // se utilizan los metodos que solo necesita que en este caso son 3
        public void Presentacion()
        {
            Console.WriteLine("El celular Huawei: ");
        }
        public void Resta()
        {
            Console.WriteLine("Puede realizar una resta");
        }

        public void Suma()
        {
            Console.WriteLine("Puede realizar una suma");
        }
    }
}
