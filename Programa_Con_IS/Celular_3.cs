﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programa_Con_IS
{
    class Celular_3: ICelular_3 //se implementa la interfaz que se necesita que es la del celular 3
    {
        // se utilizan los metodos que solo necesita que en este caso son 2
        public void Presentacion()
        {
            Console.WriteLine("El celular Xiaomi: ");
        }
        public void Division()
        {
            Console.WriteLine("Puede realizar una división");
        }
    }
}
