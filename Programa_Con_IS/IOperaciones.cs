﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programa_Con_IS
{
    //Se crea una interfaz de alto nivel para implementar las demás interfaces
    interface IOperaciones:ICelular_2, ICelular_3 // La interfaz va a tener todo aquello que este en las interfaces
    {
        void Presentacion(); // un método general que todos las clases necesita
    }
}
