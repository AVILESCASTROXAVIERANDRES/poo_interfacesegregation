﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programa_Sin_IS
{
    interface IOperaciones
    {
        void Division();
        void Suma();
        void Resta();
        void Presentacion();
    }
}
